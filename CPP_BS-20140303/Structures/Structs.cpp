#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

struct Person	
{
	int id;
	double salary;
	string name;
};

union Data
{
	int x;
	double dx;
	char c;
};

void print(const Person& p)
{
	cout << "ID: " << p.id << " " << p.name
		<< " Salary: " << p.salary << endl;
}

Person* create_person(const string& name, double salary)
{
	static int id_gen = 1;

	Person* p = new Person;
	p->id = id_gen++;
	p->name = name;
	p->salary = salary;

	return p;
}

shared_ptr<Person> create_person_by_shared_ptr(const string& name, double salary)
{
	static int id_gen = 1;

	shared_ptr<Person> p = make_shared<Person>();
	p->id = id_gen++;
	p->name = name;
	p->salary = salary;

	return p;
}

void make_report()
{
	vector<Person*> vec_employees;

	vec_employees.push_back(create_person("Nowak Anna", 4500.0));

	print(*vec_employees[0]);

	vec_employees.push_back(create_person("Prezes Antoni", 6000.0));
	print(*vec_employees[1]);

	/*while (true)
	create_person("Wyciek Krzysztof", 100000.0);*/

	// sprzatanie po i obiekcie

	for (vector<Person*>::iterator it = vec_employees.begin();
		it != vec_employees.end(); ++it)
	{
		delete *it;
	}
}

int main()
{
	Person p1;

	p1.id = 1;
	p1.name = "Kowalski Jan";
	p1.salary = 3500;

	print(p1);

	cout << "sizeof Person: " << sizeof(Person) << endl;

	cout << "sizeof Data: " << sizeof(Data) << endl;

	Data d;

	d.dx = 10;

	cout << "d.x = " << d.x << endl;  // nieprawidlowy odczyt z unii
	cout << "d.dx = " << d.dx << endl;

	// zarzadzanie pamiecia: new i delete
	Person* zus = new Person[1000000];

	delete [] zus;

	// shared_ptr

	shared_ptr<Person> sp1 = create_person_by_shared_ptr("Nijaki Zenon", 3000.0);

	print(*sp1);

	shared_ptr<Person> sp2 = create_person_by_shared_ptr("Nijaki Krzysztof", 3000.0);

	print(*sp2);

	sp2 = sp1;

	system("PAUSE");
}