#include <iostream>
#include <string>

using namespace std;

enum DayOfWeek : char { Mon = 1, Tue, Wed, Thd, Fri, Sat, Sun };

int main()
{
	int n;

	cout << "Podaj dzien tygodnia: ";
	cin >> n;

	DayOfWeek day = (DayOfWeek)n;

	cout << "Wprowadzono dzien: " << day << endl;

	switch (day)
	{
	case Mon:
		cout << "Poniedzialek\n";
		break;
	case Tue:
		cout << "Wtorek\n";
		break;
	case Wed:
		cout << "Sroda\n";
		break;
	case Thd:
		cout << "Czwartek\n";
		break;
	case Fri:
		cout << "Piatek\n";
		break;
	case Sat:
	case Sun:
		cout << "Weekend\n";
	default:
		cout << "Nieprawidlowa wartosc wyliczenia\n";
	}

	cout << "sizeof(DayOfWeek) = " << sizeof(DayOfWeek) << endl;

	system("PAUSE");
}