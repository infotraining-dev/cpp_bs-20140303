#include <iostream>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

using namespace std;

struct Point
{
	int x, y;

	Point(int x, int y) : x(x), y(y) {}
	
	void translate(int dx, int dy)
	{
		x += dx;
		y += dy;
	}
};

ostream& operator<<(ostream& out, const Point& pt)
{
	out << "[" << pt.x << ", " << pt.y << "]";
	return out;
}

// interfejs w styly C++
class Shape
{
public:
	virtual void draw() const = 0;
	virtual void move(int dx, int dy) = 0;
	virtual ~Shape() {}
};

class ShapeBase : public Shape
{
public:
	ShapeBase(int x, int y) : coord_(x, y)
	{}

	virtual void move(int dx, int dy)
	{
		coord_.translate(dx, dy);
	}
protected:
	Point coord() const { return coord_;  }
private:
	Point coord_;
};

class Circle : public ShapeBase
{
public:
	Circle(int x, int y, int radius) : ShapeBase(x, y), radius_(radius)
	{}

	int radius() const { return radius_; }
	void set_radius(int radius) { radius_ = radius; }

	void draw() const
	{
		cout << "Drawing circle at " << coord() << " with r: " << radius_ << endl;
	}

private:
	int radius_;
};

// napisac klase Rectangle
class Rectangle : public ShapeBase
{
public:
	Rectangle(int x, int y, int w, int h) : ShapeBase(x, y), width_(w), height_(h)
	{}

	int width() const { return width_; }
	void set_width(int w) { width_ = w; }
	int height() const { return height_; }
	void set_height(int h) { height_ = h; }

	void draw() const
	{
		cout << "Drawing rectangle at " << coord()
			<< " with widht: " << width_
			<< " and height: " << height_ << endl;
	}

private:
	int width_, height_;
};

class Line : public ShapeBase
{
public:
	Line(int x, int y, int x2, int y2) : ShapeBase(x, y), end_(x2, y2)
	{}

	Point end_of_line() const { return end_; }
	void set_end_of_line(const Point& p) { end_ = p; }

	void draw() const
	{
		cout << "Drawing line from " << coord() << " to " << end_ << endl;
	}

	void move(int dx, int dy)
	{
		ShapeBase::move(dx, dy);  // wywolanie metody z klasy bazowej
		end_.translate(dx, dy);
	}

private:
	Point end_;
};

class GraphicsDocument
{
	typedef boost::shared_ptr<Shape> ShapePtr;
	typedef vector<ShapePtr> VectorShapes;

public:
	void add(ShapePtr shape)
	{
		shapes_.push_back(shape);
	}

	void show_on_screen()
	{
		for (VectorShapes::const_iterator it = shapes_.begin();
			it != shapes_.end(); ++it)
			(*it)->draw();
	}

private:
	VectorShapes shapes_;
};

int main()
{
	//Shape s1(1, 5);
	//s1.draw();
	//s1.move(4, -2);
	//s1.draw();

	Circle c1(5, 3, 10);
	c1.draw();
	c1.move(-1, 1);
	c1.draw();

	Rectangle r1(10, 20, 40, 20);
	r1.set_width(10);
	r1.draw();
	r1.set_height(30);
	r1.draw();

	cout << "Te same typy dla r1 i c1: " << (typeid(r1) == typeid(c1)) << endl;

	Shape* ptr1 = &r1;
	Shape* ptr2 = &c1;

	cout << "Te same typy dla ptr1 i ptr2: " << (typeid(*ptr1) == typeid(*ptr2)) << endl;

	cout << "\nSlicing: " << endl;

	//Shape s2 = r1;  // slicing
	//s2.draw();

	cout << "\nKonwersja wskaznikow:\n";

	// wskazniki do klas bazowych i pochodnych
	Shape* ptr_shp = &r1;
	ptr_shp->draw();

	ptr_shp = &c1;
	ptr_shp->draw();

	cout << "\nGraphics doc:\n";

	GraphicsDocument doc;

	boost::shared_ptr<Shape> ptr_shape(new Line(1, 10, 100, 20));

	doc.add(boost::shared_ptr<Shape>(new Rectangle(10, 20, 100, 20)));
	doc.add(ptr_shape);
	doc.add(boost::shared_ptr<Shape>(new Circle(10, 20, 100)));

	doc.show_on_screen();

	ptr_shape->move(10, 10);

	doc.show_on_screen();

	system("PAUSE");
}