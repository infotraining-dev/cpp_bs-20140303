#include <iostream>
#include <string>

using namespace std;

int main()
{
	// oparatory arytmetyczne
	int x = 10;
	int y = 3;

	cout << "x * y = " << x * y << endl;
	cout << "x / y = " << x / (double)y << endl;
	// reszta z dzielenia - modulo
	cout << "x % y = " << x % y << endl;

	// operatory ++ --
	cout << x++ << endl;  // x = x + 1;  // pobierz i zwieksz
	cout << ++x << endl;  // zwi�ksz i pobierz
 
	// operatory przypisania
	x += 5;  // x = x + 5

	cout << boolalpha << "x == y: " << (x == y) << endl;
	cout << "x != y: " << (x != y) << endl;

	bool is_valid = true;
	bool is_complex = false;
	int is_number = 0;

	cout << "is_valid and is_complex: " << (is_valid && is_complex) << endl;
	cout << "is_valid and is_complex: " << (is_valid || is_complex) << endl;
	cout << "is_valid and is_number: " << (is_valid && is_number) << endl;

	// operatory bitowe
	unsigned int bits = 1;

	cout << (bits << 2) << endl;

	unsigned int other_bits = 7;

	cout << ~(bits & other_bits) << endl;

	// operator sizeof

	cout << "sizeof(int) = " << sizeof(int) << endl;
	cout << "sizeof(bits) = " << sizeof(bits) << endl;

	// operator ?
	x = 12;
	cout << x << " jest " << ((x % 2) ? "nieparzysta" : "parzysta") << endl;

	system("PAUSE");
}