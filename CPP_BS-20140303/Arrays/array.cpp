#include <iostream>

using namespace std;

void print(int numbers[], size_t size)
{
	for (size_t i = 0; i < size; ++i)
		cout << numbers[i] << " ";
	cout << "\n";
}

int main()
{
	int numbers[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	for (int i = 0; i < 10; ++i)
		cout << numbers[i] << " ";
	cout << "\n";

	numbers[5] = 10;
	*(numbers + 5) = -1;

	for (int* it = numbers; it != numbers + 10; ++it)
		cout << *it << " ";
	cout << "\n";

	system("PAUSE");
}
