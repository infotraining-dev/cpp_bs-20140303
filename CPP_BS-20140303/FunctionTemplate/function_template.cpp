#include <iostream>
#include <string>
#include <functional>
#include <algorithm>
#include <iterator>
#include <boost/type_traits/add_const.hpp>

using namespace std;

/*	prosty algorytm sortowania
	
	sort(tab[], rozmiar)
		for i = 0 to rozmiar-1:
			for j = i to rozmiar-1:
				if (tab[j] < tab[i]):
					swap(tab[i], tab[j])
*/

/* 1 - napisa� uog�lniony algorytm sortuj�cy (szablon funkcji) */
template <typename T>
void slow_sort(T tab[], size_t size)
{
	for (size_t i = 0; i < size; ++i)
		for (size_t j = i; j < size; ++j)
		if (tab[j] < tab[i])
				swap(tab[i], tab[j]);
}

/* 2 - napisa� szablon funkcji wypisuj�cej elementy kontenera na ekranie */
template <typename Iterator>
void print_array(Iterator first, Iterator end)
{
	cout << "[ ";
	for (Iterator it = first; it != end; ++it)
		cout << *it << " ";
	cout << "]\n";
}

/* 3 - napisa� uog�lniony algorytm sortuj�cy z komparatorem */
template <typename T, typename Comp>
void slow_sort(T tab[], size_t size, Comp comp)
{
	for (size_t i = 0; i < size; ++i)
	for (size_t j = i; j < size; ++j)
	if (comp(tab[j], tab[i]))
		swap(tab[i], tab[j]);
}

bool grt(int a, int b)
{
	return a > b;
}

struct Person
{
	int id;
	string name;
public:
	Person(int id, const string& name) : id(id), name(name)
	{}

	bool operator<(const Person& p) const
	{
		return id < p.id;
	}
};

ostream& operator<<(ostream& out, const Person& p)
{
	out << p.id << " " << p.name;
	return out;
}

bool compare_by_name(const Person& p1, const Person& p2)
{
	return p1.name < p2.name;
}

class CompareByNameDesc
{
public:
	bool operator()(const Person& p1, const Person& p2) const
	{
		return p1.name > p2.name;
	}
};

class IsEven
{
public:
	bool operator()(int n) const
	{
		return n % 2 == 0;
	}
};

int main()
{
	int numbers[10] = { 9, 5, 1, 9, 5, 6, 8, 3, 2, 0 };

	cout << "Ilosc 9: " << count(numbers, numbers + 10, 9) << endl;

	cout << "Ilosc parzystych: "
		<< count_if(numbers, numbers + 10, IsEven()) << endl;

	// C++11 lambda
	int x = 3;
	cout << "Ilosc podzielnych przez x: "
		<< count_if(numbers, numbers + 10, [=](int n) { return n % x == 0; })
		<< endl;

	print_array(numbers, numbers + 10);

	slow_sort(numbers, 10);

	print_array(numbers, numbers+10);

	string strings[5] =
		{ string("1_jeden"), string("4_cztery"), string("2_dwa"), string("8_osiem"), string("7_siedem") };

	slow_sort(strings, 5);
	
	print_array(strings, strings+5);
	
	slow_sort(numbers, 10, &grt);

	print_array(numbers, numbers + 10);
	
	const int tab_of_consts[3] = { 1, 2, 3 };
	
	print_array(&tab_of_consts[0], tab_of_consts + 3);

	Person people[4] = { Person(14, "Kowalski"), Person(3, "Zet"),
		Person(9, "Anonim"), Person(2, "Nijaki") };

	slow_sort(people, 4, &compare_by_name);

	print_array(people, people + 4);

	CompareByNameDesc comp_desc;

	cout << boolalpha << people[0] << " > " << people[1] << " - "
		<< comp_desc(people[0], people[1]) << endl;

	//slow_sort(people, 4, CompareByNameDesc());

	cout << "\Sort:\n";

	sort(people, people + 4);

	print_array(people, people + 4);

	sort(people, people + 4, CompareByNameDesc());

	print_array(people, people + 4);

	system("PAUSE");

	return 0;
}
