#include <iostream>
#include <string>

using namespace std;

int main()
{
	char ch = 65;
	ch = 'B';

	cout << "ch = " << (int)ch << endl;

	int x = 22;
	x = ch;
	
	cout << "x = " << x << endl;
	cout << "hex x = " << hex  << showbase << x << endl;
	cout << dec << "x = " << x << endl;

	double dx = 3.1415;

	cout << "dx = " << dx << endl;

	bool is_ok = true;

	cout << boolalpha << "is_ok = " << is_ok << endl;

	cout << noboolalpha <<  "!is_ok = " << (!is_ok) << endl;

	// niejawna konwersja
	short sx = -12000;
	
	ch = sx;

	cout << "ch = " << (short)ch << endl;

	unsigned int ux = sx;

	cout << "ux = " << ux << endl;

	x = (int)dx;

	cout << "x = " << x << endl;

	// typy łańcuchowe
	const char* txt = "Ala ma kota\n";

	cout << txt;

	string str = "Ala ma kota";
		
	cout << str << endl;

	cout << "Podaj x: ";

	cin >> x;

	cout << "x = " << x << endl;

	system("PAUSE");
}