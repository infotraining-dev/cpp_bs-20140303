#ifndef DYNAMIC_ARRAY_HPP
#define DYNAMIC_ARRAY_HPP

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <string>
#include <stdexcept>

namespace Utils
{
	class DynamicArray
	{
	public:
		DynamicArray(size_t size, int value = 0)
			: size_(size)
		{
			std::cout << "DynamicArray(" << size_ << ")\n";

			array_ = new int[size_];

			for (size_t i = 0; i < size_; ++i)
				array_[i] = value;
		}

		// konstruktor kopiujacy
		DynamicArray(const DynamicArray& source)
			: size_(source.size_)
		{
			std::cout << "Copy constructor DynamicArray(" 
				<< size_ << ")\n";

			array_ = new int[size_];

			// kopiowanie elementow
			for (size_t i = 0; i < size_; ++i)
				array_[i] = source.array_[i];
		}

		DynamicArray& operator=(const DynamicArray& other)
		{
			if (this != &other) // unikniecie samoprzypisania
			{
				// sprzątanie po obiekcie
				delete[] array_;
				array_ = new int[other.size_];

				// kopiowanie rozmiaru
				size_ = other.size_;

				// kopiowanie elementow
				for (size_t i = 0; i < size_; ++i)
					array_[i] = other.array_[i];
			}
			return *this;
		}

		~DynamicArray()  // destruktor
		{
			std::cout << "~DynamicArray(" << size_ << ")\n";
			delete[] array_;
		}

		size_t size() const
		{
			return size_;
		}

		int& at(size_t index)
		{
			if (index >= size_)
				throw std::out_of_range("Index out of range");

			return array_[index];
		}

		const int& at(size_t index) const
		{
			if (index >= size_)
				throw std::out_of_range("Index out of range");

			return array_[index];
		}

		int& operator[](size_t index)
		{
			return array_[index];
		}

		const int& operator[](size_t index) const
		{
			return array_[index];
		}

	private:
		int* array_;
		size_t size_;
	};

	class DynamicCharArray
	{
	public:
		DynamicCharArray(const char* text)
			: size_(strlen(text)+1)
		{
			std::cout << "DynamicArray(" << size_ << ")\n";

			array_ = new char[size_];

			strcpy_s(array_, size_, text);
		}

		// konstruktor kopiujacy
		DynamicCharArray(const DynamicCharArray& source)
			: size_(source.size_)
		{
			std::cout << "Copy constructor DynamicArray("
				<< size_ << ")\n";

			array_ = new char[size_];

			// kopiowanie elementow
			for (size_t i = 0; i < size_; ++i)
				array_[i] = source.array_[i];
		}

		~DynamicCharArray()  // destruktor
		{
			std::cout << "~DynamicArray(" << size_ << ")\n";
			delete[] array_;
		}

		size_t size() const
		{
			return size_;
		}

		char& at(size_t index)
		{
			return array_[index];
		}

		const char& at(size_t index) const
		{
			return array_[index];
		}

	private:
		char* array_;
		size_t size_;
	};

	class File
	{
		FILE* file_;
		File(const File&);
	public:
		File(const std::string& filename)
		{
			fopen_s(&file_, filename.c_str(), "w");

			if (file_ == NULL)
				throw std::runtime_error("File open error: " + filename);

			std::cout << "File(" << filename << ")\n";
		}

		void add_line(const std::string& line)
		{
			fprintf(file_, "%s\n", line.c_str());
		}

		~File()
		{
			std::cout << "~File()\n";

			fclose(file_);
		}
	};

	class Singleton	
	{
	public:
		static Singleton& instance()
		{
			if (instance_ == NULL)
				instance_ = new Singleton();

			return *instance_;
		}

		void log(const std::string& msg)
		{
			std::cout << "Log: " << msg << std::endl;
		}
	private:
		Singleton() { std::cout << "Singleton()\n"; }
		Singleton(const Singleton&);
		static Singleton* instance_;
	};
}

#endif