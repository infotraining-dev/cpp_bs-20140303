#include "DynamicArray.hpp"
#include <iostream>
#include <string>

using namespace std;
using namespace Utils;

Singleton* Singleton::instance_ = NULL;

void print(const DynamicArray& da, const string& prefix)
{
	cout << prefix << ": ";

	for (size_t i = 0; i < da.size(); ++i)
		cout << da.at(i) << " ";
	cout << "\n";
}

int main()
{
	{
		DynamicArray tab1(10);

		tab1.at(4) = 10;
		tab1[3] = 7;

		print(tab1, "tab1");

		//DynamicArray tab2 = tab1; // konstruktor kopiujacy
		DynamicArray tab2(tab1);

		tab2.at(0) = -1;

		print(tab1, "tab1 po kopiowaniu");
		print(tab2, "tab2 po kopiowaniu");

		tab2 = tab2; // operator =

		print(tab2, "tab2 po przypisaniu");

		try
		{
			tab2.at(100) = 10;

			cout << "To sie nigdy nie wyswietli" << endl;
		}
		catch (const std::out_of_range& e)
		{
			cout << "Error: " << e.what() << endl;
		}
		
	}

	cout << "After block\n";

	cout << "\n\n";
	{
		DynamicCharArray text = "text";

		cout << &text.at(0) << endl;

		text.at(0) = 'T';

		cout << &text.at(0) << endl;
	}

	cout << "After block\n";
	
	try
	{
		File file("text.txt");

		//File copy_of_file = file;  // blad - nie mozna kopiowac

		file.add_line("Line1");
		file.add_line("Line2");

		File file2("text.txt");
	}
	catch (const std::runtime_error& e)
	{
		cout << "Error - " << e.what() << endl;
	}

	Singleton::instance().log("Error");

	Singleton& logger = Singleton::instance();

	logger.log("Error");

	system("PAUSE");
}