#include <iostream>
#include <string>
#include <functional>
#include <algorithm>

using namespace std;

/*	prosty algorytm sortowania
	
	sort(tab[], rozmiar)
		for i = 0 to rozmiar-1:
			for j = i to rozmiar-1:
				if (tab[j] < tab[i]):
					swap(tab[i], tab[j])
*/

/* 1 - napisa� uog�lniony algorytm sortuj�cy (szablon funkcji) */

/* 2 - napisa� szablon funkcji wypisuj�cej elementy kontenera na ekranie */

/* 3 - napisa� uog�lniony algorytm sortuj�cy z komparatorem */


bool greater(int a, int b)
{
	return a > b;
}

int main()
{
	int numbers[10] = { 9, 5, 1, 9, 5, 6, 8, 3, 2, 0 };
	
	/* TODO - odkomentowac kod
	slow_sort(numbers, 10);

	print_array(numbers, numbers+10);

	print_array(numbers, numbers+10);

	string strings[5] =
		{ string("1_jeden"), string("4_cztery"), string("2_dwa"), string("8_osiem"), string("7_siedem") };

	slow_sort(strings, 5);
	
	print_array(strings, strings+5);
	
	slow_sort(numbers, 10, &graeater);
	*/
	
	//system("PAUSE");

	return 0;
}
