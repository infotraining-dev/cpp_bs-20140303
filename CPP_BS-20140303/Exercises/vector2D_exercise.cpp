#include <iostream>
#include <cmath>
#include <complex>

using namespace std;

/* 
ĆWICZENIE:

Napisać klasę Vector2D będącą implementacją wektora w przestrzeni dwuwymiarowej. Klasa powinna zawierać:
+ konstruktor dwuargumentowy z wartościami domyślnymi równymi zero
+ metodę length() zwracającą długość wektora
+ metody dostępowe: do odczytu wsp. x x() i do zapisu set_x() oraz do odczytu wsp. y y() i zapisu set_y()
+ metodę print() wyświetlającą wektor w formacie [1.5,0.33]   
+ metodą add(const Vector2D& v) zwracającą sumę dwóch wektorów: np. v3 = v1.add(v2)
+ metodę multiply(double a) zwracającą wektor przemnożony przez a
+ metodę multiply(const Vector2D& v) zwracającą iloczyn skalarny dwóch wektorów
*/

int main()
{
	/* Użycie klasy Vector2D
	
	const Vector2D wersor_x(1.0);  // [1.0, 0.0]
	const Vector2D wersor_y(0, 1.0);

	Vector2D vec(5.0);  // Vector2D(5.0, 0.0)
	cout << "vec: ";
	vec.print();

	cout << "\nwersor x: ";
	wersor_x.print();

	Vector2D v1(1.0, -2.0);

	cout << "\nv1: ";
	cout << v1 << endl;
	cout << "v1.length() = " << v1.length() << endl;

	Vector2D v2(4.5, 1.5);
	cout << "\nv2: ";
	v2.print();

	cout << "\nv1 + v2 = ";
	Vector2D vr1 = v1.add(v2);
	vr1.print();

	cout << "\nv1 * 3.0 = ";
	Vector2D vr2 = v1.multiply(3.0);
	vr2.print();

	*/
	
	system("PAUSE");
}