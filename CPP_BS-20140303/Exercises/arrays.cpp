#include <iostream>
#include <cstdlib>
#include <time.h>

using namespace std;

void initRandomSeed()
{
	srand( (unsigned)time( NULL ) );
}

void initWithRandomValues(int array[], size_t size, int rangeMin, int rangeMax)
{
	 for(size_t i = 0; i < size; ++i)
		 array[i] = static_cast<int>(
						static_cast<double>(rand()) / (RAND_MAX + 1) * (rangeMax - rangeMin) + rangeMin
					);
}

// Ex1 - napisz funkcj� wy�wietlaj�c� zawarto�� tablicy liczb ca�kowitych

// Ex2 - napisz funkcj� licz�c� warto�� �redni� z liczb przechowywanych w tablicy

// Ex3 - napisz funkcj� znajduj�c� warto�ci minimaln� i maksymaln� z tablicy int'�w przekazanej jako argument

int main()
{
	int values[10]; // tablica 10 element�w typu int

	// wypisanie element�w na ekranie
	cout << "[ ";
	for(int i = 0; i < 10; ++i)
		cout << values[i] << " ";
	cout << "]\n";

	initRandomSeed();
	initWithRandomValues(values, 10, 0, 50);

	// wypisanie element�w na ekranie
	cout << "[ ";
	for(int i = 0; i < 10; ++i)
		cout << values[i] << " ";
	cout << "]\n";

	return 0;
}
