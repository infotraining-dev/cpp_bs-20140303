#include <vector>
#include <string>
#include <iostream>

using namespace std;

// Ex1 - napisz funkcj� wypisuj�c� warto�ci przechowywane w wektorze

// Ex2 - napisz funkcj� transformVector przyjmuj�c� wektor liczb ca�kowitych oraz wska�nik do funkcji int (*funPtr)(int)
//       i zwracaj�c� wektor warto�ci b�d�cych wynikami dzia�ania funkcji podanej przez wska�nik

// Ex3 - napisz funkcj� licz�c� �redni� z warto�ci przechowywanych w wektorze

int main()
{
	cout << "Podaj ilosc liczb:" << endl;
	size_t n;
	cin >> n;

	int x;
	vector<int> numbers;

	// wczytanie danych
	for(size_t i = 1; i <= n; ++i)
	{
		cout << "Podaj " << i << " liczbe:\n";
		cin >> x;
		numbers.push_back(x);
	}
	
	// wypisanie tablicy
	cout << "[ ";
	for(size_t i = 0; i < numbers.size(); ++i)
		cout << numbers[i] << " ";
	cout << "]\n";
}