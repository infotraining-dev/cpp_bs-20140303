#include <iostream>

using namespace std;

// 1 - funkcja square(int x)
int square(int x)
{
	return x * x;
}

double square(double x)
{
	return x * x;
}

// Ex1 - Napisz funkcj� add(x, y) dodaj�c� dwie 
// liczby ca�kowite

int add(int x, int y)
{
	return x + y;
}

// Ex2 - Napisz funkcj� min(x, y) przyjmuj�c� dwie liczby 
// i zwracaj�c� mniejsz� z nich

int min(int a, int b)
{
	return a < b ? a : b;
}

// Ex3 - Napisz funkcj� wypisuj�c� wszystkie liczby pierwsze
// w zakresu [2; n)
bool is_prime(int number)
{
	for (int i = 2; i <= sqrt(number); ++i)
	if (number % i == 0)
		return false;

	return true;
}

void print_primes(int range)
{
	cout << "Primes in range [2; " << range << ") : ";
	for (int number = 2; number < range; ++number)
	{
		if (is_prime(number))
			cout << number << " ";
	}
	cout << "\n";
}

// Ex4 - Napisz funkcj� zwracaj�c� 
// wynik i reszt� z dzielenia 2 liczb ca�kowitych
void divide(int x, int y, int& r, int& d)
{
	r = x / y;
	d = x % y;
}

int main()
{
	int x = 5;
	cout << "square(x) = " << square(x) << endl;

	double dx = 0.25;
	cout << "square(dx) = " << square(dx) << endl; 

	int y = 10;

	cout << "x + y = " << add(x, y) << endl;
	cout << "min(x, y) = " << min(x, y) << endl;

	print_primes(100);

	int r, d;

	divide(y, 3, r, d);

	cout << "y / 3 = " << r << endl;
	cout << "y % 3 = " << d << endl;

	system("PAUSE");
}