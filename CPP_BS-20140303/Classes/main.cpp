#include <iostream>
#include <vector>
#include "bank_account.hpp"
#include "banking_exceptions.hpp"
#include <boost/shared_ptr.hpp>

using namespace std;
using namespace Banking;

int main()
{
	BankAccount ba1("Kowalski Adam", 200.0);

	print(ba1, "ba1");

	DebitAccount ba2("Nowak Jan", 4000.0);

	print(ba2, "ba2");
	
	ba1.transfer_funds(ba2, 100.0);

	cout << "\nPo przelewie:\n";

	print(ba1, "ba1");
	print(ba2, "ba2");

	try
	{
		ba1.withdraw(10000.0);
	}
	catch (const NoFundsException& e)
	{
		cout << e.what() << " Account#" << e.account_id() << endl;
	}

	cout << "\nDebit account: " << endl;

	typedef boost::shared_ptr<BankAccount> BankAccountPtr;
	typedef vector<BankAccountPtr> VectorBankAccount;

	VectorBankAccount accounts;

	accounts.push_back(BankAccountPtr(new BankAccount("Nowakowski Jerzy", 1000.0)));
	accounts.push_back(BankAccountPtr(new DebitAccount("Dluznik Jan", 2000.0, 100.0)));


	accounts[0]->withdraw(990.0);
	accounts[1]->withdraw(990.0);

	print(*accounts[0], "account[0]");
	print(*accounts[1], "account[1]");

	BankAccount* ptr_account = new BankAccount("Dluznik Anna", 500.0);

	DebitAccount* ptr_debit = dynamic_cast<DebitAccount*>(ptr_account);
	
	if (ptr_debit != NULL)
	{
		cout << "zmiana limitu\n";
		ptr_debit->set_debit_limit(1000.0);
	}
	else
		cout << "konto nie jest kontem debetowym" << endl;

	ptr_account->withdraw(300.0);

	print(*ptr_account, "po zmianie limitu i po wyplacie 700: ");

	try
	{
		DebitAccount& debit_ref = dynamic_cast<DebitAccount&>(ba1);
	}
	catch (const bad_cast& e)
	{
		cout << e.what() << endl;
	}
	
	system("PAUSE");
}