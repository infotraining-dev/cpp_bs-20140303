#ifndef BANKING_EXCEPTIONS_HPP
#define BANKING_EXCEPTIONS_HPP

#include <stdexcept>

namespace Banking
{
	class NoFundsException : public std::runtime_error
	{
	public:
		NoFundsException(const std::string& msg, int account_id) 
			: std::runtime_error(msg), account_id_(account_id)
		{}

		int account_id() const
		{
			return account_id_;
		}

	private:
		const int account_id_;
	};
}


#endif