#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <string>

namespace Banking
{
	class BankAccount
	{
		const int id_;
		std::string owner_;
		static int id_gen_;
	protected:
		double balance_;
	public:
		BankAccount(const std::string& owner, double balance = 0.0);
		
		virtual void withdraw(double amount);
		
		void deposit(double amount);

		void transfer_funds(BankAccount& target, double amount);
		
		int id() const
		{
			return id_;
		}

		double balance() const
		{
			return balance_;
		}
			
		std::string owner() const
		{
			return owner_;
		}

		void set_owner(const std::string& owner)
		{
			owner_ = owner;
		}

		virtual std::string status() const;
	};

	class DebitAccount : public BankAccount
	{
	public:
		DebitAccount(const std::string& owner, double debit_limit,
				     double balance = 0.0);						

		void withdraw(double amount);

		double debit_limit() const
		{
			return debit_limit_;
		}

		void set_debit_limit(double new_limit)
		{
			debit_limit_ = new_limit;
		}

		std::string status() const;

	private:
		double debit_limit_;
	};

	void print(const BankAccount& account, const std::string& prefix);
}

#endif