#include "bank_account.hpp"
#include "banking_exceptions.hpp"
#include <iostream>

using namespace std;

namespace Banking
{
	int BankAccount::id_gen_ = 0;

	BankAccount::BankAccount(const std::string& owner, double balance) 
		: id_(++id_gen_), owner_(owner), balance_(balance)
	{
	}

	void BankAccount::deposit(double amount)
	{
		balance_ += amount;
	}

	void BankAccount::transfer_funds(BankAccount& target, double amount)
	{
		withdraw(amount);
		target.deposit(amount);
	}

	void BankAccount::withdraw(double amount)
	{
		if (amount > balance_)
			throw NoFundsException("No funds for withdrawal!", id_);

		this->balance_ -= amount;
	}

	DebitAccount::DebitAccount(const std::string& owner, double debit_limit,
		double balance)
		: BankAccount(owner, balance),
		debit_limit_(debit_limit)
	{}

	void DebitAccount::withdraw(double amount)
	{
		if (balance() - amount < -debit_limit_)
			throw NoFundsException("No funds on debit account!", id());

		balance_ -= amount;
	}

	void print(const BankAccount& account, const std::string& prefix)
	{
		cout << prefix << ": ID = " << account.id()
			<< "; Owner = " << account.owner()
			<< "; Balance = " << account.balance() << " PLN";
		
		DebitAccount* da = 
			const_cast<DebitAccount*>(
				static_cast<const DebitAccount*>(account));
		da->withdraw(1000000.0);

		// jesli konto debetowe to wyswietl limit;
		const DebitAccount* debit_account 
			= dynamic_cast<const DebitAccount*>(&account);
		if (debit_account)
			cout << " DebitLimit = " << debit_account->debit_limit() << endl;
	}
}
