#include <iostream>
#include <string>
#include <vector>

using namespace std;

void may_throw()
{
	throw std::runtime_error("Error");
}

namespace BeforeRAII
{
	class BufferManager
	{
	public:
		BufferManager()
		{
			buffer1_ = new int[255];  // *

			try
			{
				cout << "Alloc1 ok\n";
				may_throw(); // *
				buffer2_ = new int[255];
				cout << "Alloc2 ok\n";
			}
			catch (...)
			{
				cout << "Dealloc1\n";
				delete buffer1_;
				throw;
			}

			cout << "BufferManager() completed\n";
		}

		~BufferManager()
		{
			cout << "~BufferManager()\n";
			delete[] buffer1_;
			delete[] buffer2_;
		}

	private:
		int* buffer1_;
		int* buffer2_;
	};
}

namespace RAII
{
	class BufferHandler
	{
		BufferHandler(const BufferHandler&);
	public:
		BufferHandler(int* buffer) : buffer_(buffer)
		{
			cout << "Alloc of buffer\n";
		}

		int& operator[](size_t index)
		{
			return buffer_[index];
		}

		~BufferHandler()
		{
			cout << "Dealloc of buffer\n";
			delete[] buffer_;
		}
	private:
		int* buffer_;
	};

	class BufferManager
	{
	public:
		BufferManager() : buffer1_(new int[SIZE]), buffer2_(new int[SIZE])
		{
			may_throw();
		}

		void clear()
		{
			for (int i = 0; i < SIZE; ++i)
			{
				buffer1_[i] = buffer2_[i] = 0;
			}
		}

	private:
		BufferHandler buffer1_;
		BufferHandler buffer2_;
		static const int SIZE = 255;
	};

	class BufferManagerOnVectors
	{
	public:
		BufferManagerOnVectors() : buffer1_(SIZE), buffer2_(SIZE)
		{
			may_throw();
		}

		void clear()
		{
			for (int i = 0; i < SIZE; ++i)
			{
				buffer1_[i] = buffer2_[i] = 0;
			}
		}

	private:
		vector<int> buffer1_;
		vector<int> buffer2_;
		static const int SIZE = 255;
	};
}

int main()
{
	try
	{
		RAII::BufferManager bm;
	}
	catch (const runtime_error& e)
	{
		cout << e.what() << endl;
	}

	system("PAUSE");
}