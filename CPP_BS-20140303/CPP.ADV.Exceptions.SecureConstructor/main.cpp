#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <memory>

using namespace std;

/********************************************************************
*  Uodparnianie konstrukora na wyjatki #1
********************************************************************/

class Device
{
private:
	size_t devno_;
public:
	Device(int devno) : devno_(devno)
	{
		if (devno == 2)
			throw std::runtime_error("Powazny problem!");

		cout << "Konstruktor Device #" << devno << endl;
	}

	~Device()
	{
		cout << "Destruktor Device #" << devno_ << endl;
	}
};

class Broker 
{
public:
	Broker(int devno1, int devno2) 
		: dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}

	/*Broker(int devno1, int devno2)  try : dev1_(new Device(devno1)), dev2_(new Device(devno2))
	{
	}
	catch(...)
	{
		delete dev1_;
		throw;
	}*/
private:
	std::shared_ptr<Device> dev1_;
	std::shared_ptr<Device> dev2_;
};

int main()
{
	try
	{
		Broker b(1, 2);
	}
	catch(const exception& e)
	{
		cerr << "Wyjatek: " << e.what() << endl;
	}

	system("PAUSE");
}