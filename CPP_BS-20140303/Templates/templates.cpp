#include <iostream>
#include <string>

using namespace std;

template <typename T>
const T& maks(const T& a, const T& b)
{
	return a < b ? b : a;
}

const char* maks(const char* txt1, const char* txt2)
{
	if (strcmp(txt1, txt2) < 0)
		return txt2;
	return txt1;
}

struct Person
{
	int id;
	string name;
public:
	Person(int id, const string& name) : id(id), name(name)
	{}

	bool operator<(const Person& p) const
	{
		return id < p.id;
	}
};

ostream& operator<<(ostream& out, const Person& p)
{
	out << p.id << " " << p.name;
	return out;
}

int main()
{
	int x = 10;
	int y = 20;

	cout << "maks(10, 20) = " << maks(x, y) << endl;

	double d1 = 34.14;
	double d2 = 111.14;

	cout << "maks(3.14, 1.14) = " << maks(d1, d2) << endl;

	const char* txt1 = "Ola";
	const char* txt2 = "Ala";

	cout << "maks(txt1, txt2) = " << maks(txt1, txt2) << endl;

	Person p1(1, "Kowalski");
	Person p2(2, "Nowak");

	cout << "maks(p1, p2) = " << maks(p1, p2) << endl;

	cout << "maks(x, d2) = " << maks<double>(x, d2) << endl;

	system("PAUSE");
}