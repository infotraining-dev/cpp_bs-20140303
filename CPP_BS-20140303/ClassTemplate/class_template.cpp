#include "dynamic_array.hpp"
#include <string>
#include <algorithm>

using namespace std;

template <typename Iter1, typename Iter2>
void mcopy(Iter1 first, Iter1 end, Iter2 target)
{
	while (first != end)
	{
		*(target++) = *(first++);
	}
}

namespace Metaprogramming
{
	template <unsigned long N>
	struct Factorial
	{
		static const unsigned long value = Factorial<N - 1>::value * N;
	};

	template <>
	struct Factorial<0>
	{
		static const unsigned long value = 1;
	};
}

int main()
{
	using namespace Utils;

	DynamicArray<int> tab1(10);

	tab1[6] = 4;
	tab1[3] = 1;

	for (size_t i = 0; i < tab1.size(); ++i)
		cout << tab1[i] << " ";
	cout << endl;

	DynamicArray<string> words(3);

	words[0] = "ala";
	words[1] = "ma";
	words[2] = "kota";

	for (size_t i = 0; i < words.size(); ++i)
		cout << words[i] << " ";
	cout << endl;

	sort(words.begin(), words.end());

	for (DynamicArray<string>::const_iterator it = words.begin();
		 it != words.end(); ++it)
		cout << *it << " ";
	cout << endl;

	string copy_of_words[3];

	copy(words.begin(), words.end(), copy_of_words);

	cout << "\npo kopiowaniu:\n";

	for (size_t i = 0; i < 3; ++i)
		cout << copy_of_words[i] << " ";
	cout << endl;

	using namespace Metaprogramming;
	cout << "20! = " << Factorial<20>::value << endl;

	system("PAUSE");
}