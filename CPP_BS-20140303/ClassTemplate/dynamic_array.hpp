#ifndef DYNAMIC_ARRAY_HPP
#define DYNAMIC_ARRAY_HPP

#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <string>
#include <stdexcept>

namespace Utils
{
	template <typename T>
	class DynamicArray
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		DynamicArray(size_t size, T value = T())
			: size_(size)
		{
			std::cout << "DynamicArray(" << size_ << ")\n";

			array_ = new T[size_];

			for (size_t i = 0; i < size_; ++i)
				array_[i] = value;
		}

		// konstruktor kopiujacy
		DynamicArray(const DynamicArray& source);

		DynamicArray& operator=(const DynamicArray& other)
		{
			if (this != &other) // unikniecie samoprzypisania
			{
				// sprzątanie po obiekcie
				delete[] array_;
				array_ = new T[other.size_];

				// kopiowanie rozmiaru
				size_ = other.size_;

				// kopiowanie elementow
				for (size_t i = 0; i < size_; ++i)
					array_[i] = other.array_[i];
			}
			return *this;
		}

		~DynamicArray()  // destruktor
		{
			std::cout << "~DynamicArray(" << size_ << ")\n";
			delete[] array_;
		}

		iterator begin()
		{
			return array_;
		}

		const_iterator begin() const
		{
			return array_;
		}

		iterator end()
		{
			return array_ + size_;
		}

		const_iterator end() const
		{
			return array_ + size_;
		}

		size_t size() const
		{
			return size_;
		}

		T& at(size_t index)
		{
			if (index >= size_)
				throw std::out_of_range("Index out of range");

			return array_[index];
		}

		const T& at(size_t index) const
		{
			if (index >= size_)
				throw std::out_of_range("Index out of range");

			return array_[index];
		}

		T& operator[](size_t index)
		{
			return array_[index];
		}

		const T& operator[](size_t index) const
		{
			return array_[index];
		}

	private:
		T* array_;
		size_t size_;
	};

	template <typename T>
	DynamicArray<T>::DynamicArray(const DynamicArray& source)
		: size_(source.size_)
	{
		std::cout << "Copy constructor DynamicArray("
			<< size_ << ")\n";

		array_ = new T[size_];

		// kopiowanie elementow
		for (size_t i = 0; i < size_; ++i)
			array_[i] = source.array_[i];
	}
}

#endif