#include <iostream>
#include <cmath>
#include <complex>

using namespace std;

/*
�WICZENIE:

Napisa� klas� Vector2D b�d�c� implementacj� wektora w przestrzeni dwuwymiarowej.
Klasa powinna zawiera�:
+ konstruktor dwuargumentowy z warto�ciami domy�lnymi r�wnymi zero
+ metod� length() zwracaj�c� d�ugo�� wektora
+ metody dost�powe: do odczytu wsp. x x() i do zapisu set_x() oraz do
  odczytu wsp. y y() i zapisu set_y()
+ metod� print() wy�wietlaj�c� wektor w formacie [1.5,0.33]
+ metod� add(const Vector2D& v) zwracaj�c� sum� dw�ch wektor�w: np. v3 = v1.add(v2)
+ metod� multiply(double a) zwracaj�c� wektor przemno�ony przez a
+ metod� multiply(const Vector2D& v) zwracaj�c� iloczyn skalarny dw�ch wektor�w
*/

#include "vector2D.hpp"

int main()
{
	 // U�ycie klasy Vector2D
	using namespace Geometry;

	Vector2D vec(5.0);  // Vector2D(5.0, 0.0)
	cout << "vec: ";
	vec.print();

	cout << "\nwersor x: ";
	Vector2D::versor_x.print();

	Vector2D v1(1.0, -2.0);

	cout << "\nv1: ";
	//cout << v1 << endl;
	v1.print();
	cout << "v1.length() = " << v1.length() << endl;

	Vector2D v2(4.5, 1.5);
	cout << "\nv2: ";
	v2.print();

	cout << "\nv1 + v2 = ";
	Vector2D vr1 = v1 + v2;
	v1.add(v2).print();
	vr1.print();

	cout << "\nv1 * v2 = " << (v1 * v2) << endl;

	cout << "\nv1 * 3 = " << (v1 * 3) << endl;

	cout << "\nv1 * 3.0 = ";
	Vector2D vr2 = v1.multiply(3.0);
	vr2.print();

	system("PAUSE");
}