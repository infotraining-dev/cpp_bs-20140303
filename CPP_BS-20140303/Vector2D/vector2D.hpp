#ifndef VECTOR2D_HPP
#define VECTOR2D_HPP

#include <iostream>

namespace Geometry
{
	class Vector2D
	{
	public:
		explicit Vector2D(double x = 0.0, double y = 0.0);

		inline double x() const;

		void set_x(double x)
		{
			x_ = x;
		}

		double y() const { return y_; }
		void set_y(double y) { y_ = y; }
		double length() const;
		Vector2D add(const Vector2D& vec) const;
		Vector2D operator +(const Vector2D& vec) const;
		Vector2D multiply(double a) const;
		double multiply(const Vector2D& vec) const;
		
		Vector2D operator-(const Vector2D& v) const
		{
			return Vector2D(x_ - v.x_, y_ - v.y_);
		}

		Vector2D operator-() const
		{
			return Vector2D(-x_, -y_);
		}

		void print() const;

		const static Vector2D versor_x;
		const static Vector2D versor_y;
	private:
		double x_, y_;
	};

	double operator*(const Vector2D& v1, const Vector2D& v2);
	Vector2D operator*(const Vector2D& v, double d);

	std::ostream& operator <<(std::ostream& out, const Vector2D& vec);

	double Vector2D::x() const
	{
		return x_;
	}
}

#endif

