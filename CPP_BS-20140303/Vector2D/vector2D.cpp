#include "vector2D.hpp"
#include <iostream>
#include <cmath>

using namespace std;

namespace Geometry
{
	// definicja składowych statycznych
	const Vector2D Vector2D::versor_x(1.0);
	const Vector2D Vector2D::versor_y(0.0, 1.0);

	Vector2D::Vector2D(double x, double y) : x_(x), y_(y)
	{
	}

	double Vector2D::length() const
	{
		return sqrt(x_ * x_ + y_ * y_);
	}

	Vector2D Vector2D::add(const Vector2D& vec) const
	{
		return Vector2D(x_ + vec.x_, y_ + vec.y_);
	}

	Vector2D Vector2D::operator+(const Vector2D& vec) const
	{
		return Vector2D(x_ + vec.x_, y_ + vec.y_);
	}

	Vector2D Vector2D::multiply(double a) const
	{
		return Vector2D(x_ * a, y_ * a);
	}

	void Vector2D::print() const
	{
		cout << "[" << x_ << "," << y_ << "]" << endl;
	}

	double Vector2D::multiply(const Vector2D& vec) const
	{
		return x_ * vec.x_ + y_ * vec.y_;
	}

	double operator*(const Vector2D& v1, const Vector2D& v2)
	{
		return v1.x() * v2.x() + v1.y() * v2.y();
	}

	Vector2D operator*(const Vector2D& v, double d)
	{
		return Vector2D(v.x() * d, v.y() * d);
	}

	std::ostream& operator <<(std::ostream& out, const Vector2D& vec)
	{
		out << "[" << vec.x() << ", " << vec.y() << "]";

		return out;
	}
}