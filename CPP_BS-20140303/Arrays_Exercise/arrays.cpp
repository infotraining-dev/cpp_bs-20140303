﻿#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

void initRandomSeed()
{
	srand((unsigned)time(NULL));
}

void initWithRandomValues(int array[], size_t size, int rangeMin, int rangeMax)
{
	for (size_t i = 0; i < size; ++i)
		array[i] = 
			static_cast<int>(
				static_cast<double>(rand()) / (RAND_MAX + 1) * (rangeMax - rangeMin) + rangeMin
		);
}

// Ex1 - napisz funkcję wywietlajšcš zawartoć tablicy liczb całkowitych
void print_array(int array[], size_t size)
{
	// wypisanie elementów na ekranie
	cout << "[ ";
	for (size_t i = 0; i < size; ++i)
		cout << array[i] << " ";
	cout << "]\n";
}


// Ex2 - napisz funkcję liczaca 
// wartosc srednia z liczb przechowywanych w tablicy
double avg(int array[], size_t size)
{
	double sum = 0.0;

	for (int* it = array; it != array + size; ++it)
		sum += *it;

	return sum / size;
}

void min_max(int array[], size_t size, int& min, int& max)
{
	min = max = array[0];

	for (size_t i = 1; i < size; ++i)
	{
		if (array[i] < min)
			min = array[i];
		else if (array[i] > max)
			max = array[i];
	}
}

bool is_palindrom(const char* text, size_t size)
{
	size_t i1 = 0;
	size_t i2 = size - 1;

	while (i1 < i2)
	{
		if (text[i1++] != text[i2--])
		{
			return false;
		}
	}

	return true;
}

bool is_palindrom(const string& text)
{
	return is_palindrom(text.c_str(), text.size());
}

bool is_palindrom(const char* first, const char* last)
{
	--last;
	while (first < last)
	{
		if (*(first++) != *(last--))
			return false;
	}

	return true;
}

// Ex3 - napisz funkcję znajdujšcš wartoci 
// minimalnš i maksymalnš z tablicy int'ów przekazanej jako argument

int main()
{
	const int SIZE = 10;
	int values[SIZE]; // tablica 10 elementów typu int

	print_array(values, SIZE);

	initRandomSeed();
	initWithRandomValues(values, SIZE, 0, 50);

	// wypisanie elementów na ekranie
	print_array(values, SIZE);

	double mean = avg(values, SIZE);

	cout << "avg = " << mean << endl;

	int min, max;
	min_max(values, SIZE, min, max);

	cout << "min = " << min << "; max = " << max << endl;

	cout << "\nPodaj tekst: ";
	string text;
	cin >> text;

	if (is_palindrom(text.c_str(), text.c_str() + text.size()))
		cout << text << " jest palindromem." << endl;
	else
		cout << text << " nie jest palindromem" << endl;

	system("PAUSE");

	return 0;
}