#include <iostream>
#include <string>

using namespace std;

int main()
{
	const double pi = 3.1415;

	const string company_name = "Ericpol";

	//company_name[0] = 'e';

	cout << pi << " " << company_name << endl;

	const char* text1 = "text";

	//text[0] = 'T';

	cout << text1 << endl;

	char text2[255];

	strcpy_s(text2, text1);

	cout << text1 << " - " << text2 << endl;

	string str1 = "String";
	str1[0] = 's';

	const string str2 = " Inny string";

	string str3 = str1 + " " + str2;

	cout << str3 << endl;

	cout << "str3 length = " << str3.size() << endl;

	str3[5] = '\0';  // uwaga!!! - nieprawidłowy stan obiektu

	cout << "str3 length = " << str3.size() << endl;
	cout << "strlen(str3) = " << strlen(str3.c_str()) << endl;

	cout << str3 << endl;

	const char* txt3 = str3.c_str();

	cout << sizeof(string) << endl;

	string str_empty = "a";

	cout << str_empty << endl;

	str_empty += "tekst";

	system("PAUSE");
}