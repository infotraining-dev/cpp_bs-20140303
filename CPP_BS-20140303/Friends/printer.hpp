#ifndef PRINTER_HPP
#define PRINTER_HPP

class Person;

class Printer
{
public:
	void print(const Person& p);
	void pretty_print(const Person& p);
};

#endif
