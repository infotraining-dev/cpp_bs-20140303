#pragma once

#include <string>

class Person
{
public:
	Person(int id, const std::string& name) : id(id), name(name)
	{}
private:
	int id;
	std::string name;

	friend void print(const Person&);
	/*friend void Printer::print(const Person& p);
	friend void Printer::pretty_print(const Person& p);*/
	friend class Printer;
};

