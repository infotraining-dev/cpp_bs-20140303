#include "printer.hpp"
#include "person.hpp"
#include <iostream>

using namespace std;

void Printer::print(const Person& p)
{
	cout << p.id << " " << p.name << endl;
}

void Printer::pretty_print(const Person& p)
{
	cout << "===========================\n"
		 << " + ID: " << p.id << "\n"
		 << " + Name: " << p.name << "\n" 
		 << "===========================" << endl;
}