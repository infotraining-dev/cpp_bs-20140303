#include <iostream>
#include <string>
#include "printer.hpp"
#include "person.hpp"

using namespace std;

void print(const Person& p)
{
	cout << p.id << " " << p.name << endl;
}

int main()
{
	Person p(1, "Kowalski");
	
	print(p);

	Printer prn;
	prn.print(p);
	prn.pretty_print(p);

	system("PAUSE");
}