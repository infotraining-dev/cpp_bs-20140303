#include <iostream>
#include <string>

using namespace std;	

void print(const string& text)
{
	cout << "Printing: " << text << endl;
}

int main()
{
	int x = 10;

	int& ref_x = x;

	cout << "x = " << x << endl;

	ref_x += 5;

	cout << "x = " << x << endl;

	string str = "Text";

	string& ref_str = str;

	ref_str += " with ref";

	cout << str << endl;

	const string& ref_to_temp = string("Temp");

	print(str);

	system("PAUSE");
}