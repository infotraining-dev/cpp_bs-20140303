#include <iostream>
#include <string>

using namespace std;

void divide(int x, int y, int* r, int* d)
{
	*r = x / y;
	*d = x % y;
}

void divide(int x, int y, int& r, int& d)
{
	r = x / y;
	d = x % y;
}

void log(const string& message)
{
	cout << "Log: " << message << endl;
}

void sms(const string& message)
{
	cout << "SMS: " << message << endl;
}

bool save_to_db(int id, const string& value, void(*on_error)(const string& message))
{
	if (id == 13) // pechowe id
	{
		on_error("Pechowe id");
		return false;
	}

	// ok
	return true;
}

int square(int x)
{
	return x * x;
}

int cube(int x)
{
	return x * x * x;
}

int main()
{
	int x = 10;
	int* ptr_int = NULL;
	
	void* raw_mem = &x;
	
	ptr_int = (int*)raw_mem;

	cout << *ptr_int << endl;

	if (ptr_int)
		cout << *ptr_int << endl;

	cout << "x = " << *ptr_int << endl;

	(*ptr_int) += 2;

	cout << "x = " << x << endl;

	int y = 5;

	int r, d;

	divide(x, y, &r, &d);

	cout << "r = " << r << "; d =  " << d << endl;

	// wskazniki do funkcji
	int(*fun_math)(int) = square;

	cout << "fun_math z square(10) = " << fun_math(10) << endl;

	fun_math = &cube;

	cout << "fun_math z cube(10) = " << fun_math(10) << endl;

	// parametryzacja przy pomocy wskaznika do funkcji

	save_to_db(13, "Jan Kowalski", &log);
	save_to_db(13, "Adam Nowak", &sms);

	system("PAUSE");
}
