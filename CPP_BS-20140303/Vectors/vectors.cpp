#include <iostream>
#include <vector>
#include <string>
#include <deque>
#include <algorithm>
#include <array>
#include <list>
#include <set>
#include <map>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& prefix = "")
{
	cout << prefix << " [ ";
	for (typename Container::const_iterator it = cont.begin();
		it != cont.end(); ++it)
		cout << *it << " ";
	cout << "]\n";
}

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
	out << "(" << p.first << ", " << p.second << ")";
	return out;
}

vector<int> min_max(const vector<int>& vec)
{
	vector<int> results(2);

	int min, max;

	min = max = vec[0];

	for (int i = 1; i < vec.size(); ++i)
	{
		if (vec[i] < min)
			min = vec[i];
		if (vec[i] > max)
			max = vec[i];
	}
	results[0] = min;
	results[1] = max;

	return results;
}

int main()
{
	vector<int> vec_int(10); // pusty wektor

	cout << "size: " << vec_int.size() << endl;

	vec_int.push_back(1);
	vec_int.push_back(2);
	vec_int.push_back(3);

	vec_int.insert(vec_int.begin() + 2, -1);

	cout << "size: " << vec_int.size() << endl;

	print(vec_int, "before");

	vec_int.resize(15);

	print(vec_int, "after resize");

	vector<int> vec_copy = vec_int;

	vector<int> minmax = min_max(vec_copy);

	cout << minmax[0] << " " << minmax[1] << endl;

	cout << "\nSize i capacity:\n";

	const int SIZE = 30;

	vector<double> vecd;

	vecd.reserve(SIZE);

	cout << "Size: " << vecd.size() << " Capacity: " << vecd.capacity() << endl;

	for (int i = 1; i <= SIZE; ++i)
	{
		vecd.push_back(i);
		cout << "Size: " << vecd.size() << " Capacity: " << vecd.capacity() << endl;
	}
		
	cout << "\n";

	vector<bool> vec_bool(16);

	vec_bool[0] = true;
	vec_bool[3] = 1;

	print(vec_bool, "vec_bool");

	vec_bool[6].flip();
	vec_bool.flip();

	print(vec_bool, "vec_bool");

	cout << "\n";

	print(vecd, "vecd");

	vector<double> half1(vecd.begin(), vecd.begin() + vecd.size() / 2);
	print(half1, "half1");

	deque<double> half2_reversed(vecd.rbegin(), 
		vecd.rbegin() + ((vecd.size() / 2) + ((vecd.size() % 2) ? 1 : 0)));
	print(half2_reversed, "half2_reversed");
	
	half2_reversed.push_front(-1);

	sort(half2_reversed.begin(), half2_reversed.end());

	print(half2_reversed, "posortowane");

	array<int, 8> tab = { 4, 1, 6, 8, 10, 2, 8, 5 };

	sort(tab.begin(), tab.end());

	print(tab, "tab");

	cout << "\n";

	list<int> lst1(half2_reversed.begin(), half2_reversed.end());

	print(lst1, "lst1");

	list<int>::iterator pos = find(lst1.begin(), lst1.end(), 23);

	if (pos != lst1.end())
		*pos = 30;

	lst1.insert(pos, tab.begin(), tab.end());

	print(lst1, "lst1 po");

	lst1.sort();

	print(lst1, "lst1 po sort");

	lst1.remove_if([](int n) { return n % 2; });

	print(lst1, "lst1 bez nieparzystych");

	set<int> set_int;

	set_int.insert(9);
	set_int.insert(3);
	set_int.insert(13);
	set_int.insert(6);

	cout << "czy jest 13? " << set_int.count(13) << endl;

	print(set_int, "set_int");

	map<int, string> months;

	months.insert(make_pair(1, "January"));
	months.insert(make_pair(2, "February"));
	months.insert(make_pair(3, "March"));

	cout << "months[1] = " << months[1] << endl;

	months[5] = "May";

	map<int, string>::iterator where = months.find(5);
	if (where != months.end())
		cout << "months[5] = " << where->second << endl;
	else
		cout << "brak 5\n";

	print(months, "months");

	system("PAUSE");
}