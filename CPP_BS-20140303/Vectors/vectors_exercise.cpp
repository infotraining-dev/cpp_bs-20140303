﻿#include <vector>
#include <string>
#include <iostream>

using namespace std;

int square(int x)
{
	return x * x;
}

// Ex1 - napisz funkcję wypisujaca wartosci przechowywane w wektorze
void print(const vector<int>& vec, const string& prefix = "")
{
	cout << prefix << " [ ";
	for (size_t i = 0; i < vec.size(); ++i)
		cout << vec[i] << " ";
	cout << "]\n";
}

// Ex2 - napisz funkcję transformVector przyjmujaca wektor liczb całkowitych 
// oraz wskanik do funkcji int (*funPtr)(int)
// i zwracajaca wektor wartoci będacych wynikami działania funkcji podanej przez wskaznik

typedef vector<int> VecInt;

VecInt transformVector(const VecInt& vec, int(*funPtr)(int))
{
	VecInt results(vec.size());

	for (size_t i = 0; i < vec.size(); ++i)
		results[i] = funPtr(vec[i]);

	return results;
}

// Ex3 - napisz funkcję liczaca srednia z wartosci przechowywanych w wektorze
double avg(const vector<int>& vec)
{
	double sum = 0.0;

	//for (int i = 0; i < vec.size(); ++i)
	//	sum += vec[i];

	for (vector<int>::const_iterator it = vec.begin(); 
		it != vec.end(); ++it)
		sum += *it;

	return sum / vec.size();
}

int main()
{
	cout << "Podaj ilosc liczb:" << endl;
	size_t n;
	cin >> n;

	int x;
	vector<int> numbers;

	// wczytanie danych
	for (size_t i = 1; i <= n; ++i)
	{
		cout << "Podaj " << i << " liczbe:\n";
		cin >> x;
		numbers.push_back(x);
	}

	// wypisanie tablicy
	print(numbers, "numbers");

	vector<int> squares = transformVector(numbers, &square);

	print(squares, "squares");

	cout << "avg for squares: " << avg(squares) << endl;

	// C++11
	vector<int> data = { 1, 2, 3, 4, 5 };

	print(data, "data");

	auto results = transformVector(data, &square);

	for (auto it = results.rbegin(); it != results.rend(); ++it)
		cout << *it << " ";
	cout << "\n";

	for (auto val : results)
	{
		cout << val << " ";
	}
	cout << "\n";

	string words[3] = { "one", "two", "three" };

	for (const auto& w : words)
	{
		cout << w << endl;
	}

	system("PAUSE");
}