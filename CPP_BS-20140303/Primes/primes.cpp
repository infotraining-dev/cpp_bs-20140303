#include <iostream>
#include "primes.hpp"

using namespace std;

namespace Primes
{
	int range;

	bool is_prime(int number)
	{
		for (int i = 2; i <= sqrt(number); ++i)
		if (number % i == 0)
			return false;

		return true;
	}
}

void Primes::print_primes(int range)
{
	cout << "Primes in range [2; " << range << ") : ";
	for (int number = 2; number < range; ++number)
	{
		if (is_prime(number))
			cout << number << " ";
	}
	cout << "\n";
}

