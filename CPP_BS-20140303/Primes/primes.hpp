#ifndef PRIMES_HPP
#define PRIMES_HPP

namespace Primes
{
	// deklaracja funkcji
	bool is_prime(int number);

	void print_primes(int range);

	extern int range;
}

#endif
