#include <iostream>
#include <string>

using namespace std;

int main()
{
	int x = 0;

	cout << "Podaj x: ";
	cin >> x;

	if (x % 2 == 0)
		cout << x << " jest parzysta" << endl;
	else
		cout << x << " jest nieparzysta\n";

	size_t day_of_week; // unsigned int

	cout << "Podaj dzien tygodnia: ";
	cin >> day_of_week;

	switch (day_of_week)
	{
	case 1:
		cout << "Pon\n";
		break;
	case 2:
		cout << "Wt\n";
		break;
	case 3:
		cout << "Sr\n";
		break;
	case 6:
	case 7:
		cout << "Weekend\n";
		break;
	default:
		cout << "Inny dzien tygodnia\n";
	};

	for (int i = 0; i < 10; i += 2)
	{
		if (i == 6)
			continue;
		cout << "P�tla nr " << i << endl;
	}

	system("PAUSE");
}